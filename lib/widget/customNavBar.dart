import 'package:clip_shadow/clip_shadow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_final_project/pages/moreScreen.dart';
import '../const/color.dart';
import '../const/helper.dart';
import '../pages/homepage.dart';
import '../pages/menupage.dart';
import '../pages/moreScreen.dart';
import '../pages/profileScreen.dart';
import '../pages/offerScreen.dart';


class CustomNavBar extends StatelessWidget {
  final bool home;
  final bool menu;
  final bool offer;
  final bool profile;
  final bool more;

  const CustomNavBar(
      {Key ?key,
        this.home = false,
        this.menu = false,
        this.offer = false,
        this.profile = false,
        this.more = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: Helper.getScreenWidth(context),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: ClipShadow(
              boxShadow: [
                BoxShadow(
                  color: AppColor.placeholder,
                  offset: Offset(
                    0,
                    -5,
                  ),
                  blurRadius: 10,
                ),
              ],
              clipper: CustomNavBarClipper(),
              child: Container(
                height: 60,
                width: Helper.getScreenWidth(context),
                padding: const EdgeInsets.symmetric(horizontal: 20),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>const MenuPage()));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          menu
                              ? Image.asset(
                            Helper.getAssetName(
                                "more_filled.png", "icon"),
                          )
                              : Image.asset(
                            Helper.getAssetName("more.png", "icon"),
                          ),
                          menu
                              ? Text("Menu",
                              style: TextStyle(color: AppColor.orange))
                              : Text("Menu"),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>const OfferScreen()));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          offer
                              ? Image.asset(
                            Helper.getAssetName(
                                "bag_filled.png", "icon"),
                          )
                              : Image.asset(
                            Helper.getAssetName("bag.png", "icon"),
                          ),
                          offer
                              ? Text("Promotions",
                              style: TextStyle(color: AppColor.orange))
                              : Text("Promotions"),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>const ProfileScreen()));

                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          profile
                              ? Image.asset(
                            Helper.getAssetName(
                                "user_filled.png", "icon"),
                          )
                              : Image.asset(
                            Helper.getAssetName("user.png", "icon"),
                          ),
                          profile
                              ? Text("Profile",
                              style: TextStyle(color: AppColor.orange))
                              : Text("Profile"),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>const MoreScreen()));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          more
                              ? Image.asset(
                            Helper.getAssetName(
                                "menu_filled.png", "icon"),
                          )
                              : Image.asset(
                            Helper.getAssetName("menu.png", "icon"),
                          ),
                          more
                              ? Text("Profile",
                              style: TextStyle(color: AppColor.orange))
                              : Text("More"),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: SizedBox(
              height: 70,
              width: 70,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: home ? AppColor.orange : AppColor.placeholder,
                onPressed: () {
                  if (!home) {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>const HomePage()));
                  }
                },
                child: Image.asset(
                    Helper.getAssetName("home_white.png", "icon")),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class CustomNavBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(size.width * 0.3, 0);
    path.quadraticBezierTo(
      size.width * 0.375,
      0,
      size.width * 0.375,
      size.height * 0.1,
    );
    path.cubicTo(
      size.width * 0.4,
      size.height * 0.8,
      size.width * 0.6,
      size.height * 0.9,
      size.width * 0.625,
      size.height * 0.1,
    );
    path.quadraticBezierTo(
      size.width * 0.625,
      0,
      size.width * 0.7,
      0.1,
    );
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
