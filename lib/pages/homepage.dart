import 'package:flutter/material.dart';
import 'package:flutter_final_project/pages/popularpage.dart';
import '../const/color.dart';
import '../const/helper.dart';
import 'package:page_transition/page_transition.dart';
import '../data/foodlist.dart';
import '../widget/customNavBar.dart';
import 'fooddetail.dart';
import 'myOrderScreen.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title:Text("Food Delivery"),
        backgroundColor: AppColor.orange,
      ),
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Hi User",style: Helper.getTheme(context).headline5),
                        IconButton(onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>const MyOrderScreen()));
                        }, icon: Icon(Icons.shopping_cart))
                      ],
                    ),
                  ),
                  SizedBox(height: 20,),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text("Delivering to", style: TextStyle(fontSize: 15),),
                  ),
                  SizedBox(height: 10,),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: DropdownButtonHideUnderline(
                        child: SizedBox(
                          width: 240,
                          child:
                          DropdownButton(
                          value: "current location",
                          style: Helper.getTheme(context).headline4,
                          items: [
                            DropdownMenuItem(child: Text("Current Location"),value: "current location",)
                          ], onChanged: (String? value) { },),
                        ),
                      )),
                  SizedBox(height: 10,),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      decoration: ShapeDecoration(shape: StadiumBorder(),color: AppColor.placeholderBg),
                      child: TextField(decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.search),
                          hintText: "Search Food",
                        hintStyle: TextStyle(color: AppColor.placeholder,fontSize: 18),
                        contentPadding: const EdgeInsets.only(top: 17)
                      ),),
                    ),),
                  SizedBox(height: 10,),
                  //Category
                  Container(
                    width: double.infinity,
                    height: 150,
                    padding: const EdgeInsets.only(left: 20),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("hamburger.jpg", "food"),
                              fit: BoxFit.cover,
                            ),
                            name: "Hamburger",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("bakery.jpg", "food"),
                              fit: BoxFit.cover,
                            ),
                            name: "Bakery",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("pizza.jpg", "food"),
                              fit: BoxFit.cover,
                            ),
                            name: "Pizza",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("western.png", "food"),
                              fit: BoxFit.cover,
                            ),
                            name: "Fried Chicken",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("rice.jpg", "food"),
                              fit: BoxFit.cover,
                            ),
                            name: "Rice",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("milktea.jpg", "drink"),
                              fit: BoxFit.cover,
                            ),
                            name: "Milktea",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("tea.jpg", "drink"),
                              fit: BoxFit.cover,
                            ),
                            name: "Tea",
                          ),
                          SizedBox(width: 10,),
                          CategoryCard(
                            image: Image.asset(
                              Helper.getAssetName("coffee.jpg", "drink"),
                              fit: BoxFit.cover,
                            ),
                            name: "Coffee",
                          ),
                          SizedBox(width: 10,),
                        ]
                     ,),
                    ),
                  ),
                  SizedBox(height: 50,),
                  //Popular Restaurant
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text("Popular Restaurant", style: Helper.getTheme(context).headline5,),
                            TextButton(onPressed:  (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>const PopularPage()));
                            },
                              child: Text("View All"),
                              style:ButtonStyle(
                                  foregroundColor: MaterialStateProperty.all(AppColor.orange)
                              ) ,
                            ),
                          ],
                        ),
                      SizedBox(height: 20,),
                      RestaurantCard(
                        image:Image.asset(Helper.getAssetName("4p.png", "restaurant"),fit: BoxFit.contain,),
                        name: "Pizza 4Ps",
                        rating:"4.5",
                        type: "Italian Food",
                      ),
                      RestaurantCard(image:Image.asset(Helper.getAssetName("kfc.png", "restaurant"),fit: BoxFit.contain,),
                        name:"KFC" ,
                        rating:"3.9",
                        type: "Fast Food",
                      ),
                      RestaurantCard(image:Image.asset(Helper.getAssetName("katinat.png", "restaurant"),fit: BoxFit.contain,),
                          name:"Katinat Coffee" ,
                          rating:"4.9",
                          type: "Coffee shop",
                      ),
                      RestaurantCard(image:Image.asset(Helper.getAssetName("phuclong.png", "restaurant"),fit: BoxFit.contain,),
                          name:"Phuc Long Tea" ,
                          rating:"4.0",
                          type: "Tea House",
                      ),
                    ],),
                  ),
                  SizedBox(height: 20,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Most Popular",
                          style: Helper.getTheme(context).headline5,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>const PopularPage()));

                          },
                          child: Text("View all", style: TextStyle(color: AppColor.orange),),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20,),
                  //Popular Items
                  Container(
                    height: 280,
                    width: double.infinity,
                    padding: const EdgeInsets.only(left: 20),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          MostPopularCard(
                            image: Image.asset(
                              Helper.getAssetName("luckytea.jpg", "drink"),
                              fit: BoxFit.cover,
                            ),
                            name: "Lucky Tea",
                            type1: "Tea House",
                            type2: "Phuc Long",
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          MostPopularCard(
                            name: "KFC Suchii",
                            image: Image.asset(
                              Helper.getAssetName("kfcphomai.jpg", "food"),
                              fit: BoxFit.cover,
                            ),
                            type1: "Fried Chicken",
                            type2: "KFC",

                          ),
                          MostPopularCard(
                            name: "Pizza 4 chees",
                            image: Image.asset(
                              Helper.getAssetName("pizza4cheese.png", "food"),
                              fit: BoxFit.cover,
                            ),
                            type1: "Pizza",
                            type2: "Pizza 4P's",

                          ),
                          MostPopularCard(
                            name: "Bubble Milk Tea",
                            image: Image.asset(
                              Helper.getAssetName("bubble_milk_tea.jpg", "drink"),
                              fit: BoxFit.cover,
                            ),
                            type1: "Milk Tea",
                            type2: "Koi Thé",

                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Delicacies near you",
                          style: Helper.getTheme(context).headline5,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>const PopularPage()));
                          },
                          child: Text("View all"),
                          style:ButtonStyle(
                              foregroundColor: MaterialStateProperty.all(AppColor.orange)
                          ) ,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20,),
                  //Delicacies
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Column(
                      children: List.generate(delicacieslist.length, (index) {
                        return Padding(
                            padding: const EdgeInsets.only(left:30,right: 30,bottom: 10),
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, PageTransition(
                                    child: FoodDetail(
                                      name: delicacieslist[index]['name'],
                                      type2: delicacieslist[index]['type2'],
                                      img: delicacieslist[index]['img'],
                                      description: delicacieslist[index]['description'],

                                    ),
                                    type: PageTransitionType.scale));

                              },
                              child: Row(

                                children: [
                                  Container(
                                    width: (Helper.getScreenWidth(context)-100),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                            Row(
                                              children: [
                                                CircleAvatar(backgroundImage: AssetImage(delicacieslist[index]['img']),radius: 30,),
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(" "+delicacieslist[index]['name'],style: Helper.getTheme(context).headline4,),
                                                    Text(" "+delicacieslist[index]['type1'] +"."+ delicacieslist[index]['type2'],),
                                                    Row(
                                                      children: [
                                                        Icon(Icons.star,color: AppColor.orange,),
                                                        Text(
                                                          "4.9",
                                                          style: TextStyle(
                                                            color: AppColor.orange,
                                                          ),
                                                        ),
                                                        Text("{100+ rating}"),
                                                      ],
                                                    ),
                                                  ],
                                                ),

                                              ],
                                            ),

                                      ],
                                    ) ,


                                  )
                                ],
                              ),

                            ),

                        );
                      }),
                    )
                  )
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                home: true,
              )),
        ],
      ),
    );
  }
}


class CategoryCard extends StatelessWidget {
  const CategoryCard({
    Key? key,
    required Image image,
    required String name,
  })  : _image = image,
        _name = name,
        super(key: key);

  final String _name;
  final Image _image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            width: 100,
            height: 100,
            child: _image,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(_name,style: Helper.getTheme(context).headline4?.copyWith(color: AppColor.primary),
        )
      ],
    );
  }
}
class RestaurantCard extends StatelessWidget {
  const RestaurantCard({
    Key? key,
    required Image image,
    required String name,
    required String rating,
    required String type,
  }) :_type=type,_rating=rating,_image = image,_name = name,super(key: key);
  final String _name;
  final Image _image;
  final String _rating;
  final String _type;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          width: double.infinity,
          child:Column(
            children: [
              Container(
                  child: _image),
              Column(
                children: [
                  Row(
                    children: [
                      Text(_name,style: Helper.getTheme(context).headline3,),
                    ],),
                  Row(
                    children: [
                      Icon(Icons.star,color: AppColor.orange,),
                      SizedBox(width:5,),
                      Text(_rating,style: TextStyle(color: AppColor.orange)),
                      SizedBox(width:5,),
                      Text("{100+ rating}"),
                      SizedBox(width:5,),
                      Text(_type,style: Helper.getTheme(context).headline3,)
                    ],
                  )


                ],
              )
            ],

          ) ),
    );
  }
}
class MostPopularCard extends StatelessWidget {
  const MostPopularCard({
    Key? key,
    required String name,
    required Image image,
    required String type1,
    required String type2,
  })  : _name = name,
        _image = image,
        _type1 = type1,
        _type2 = type2,
        super(key: key);

  final String _name;
  final Image _image;
  final String _type1;
  final String _type2;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            alignment: Alignment.bottomLeft,
            width: 300,
            height: 200,
            child: _image,
          ),
        ),
        SizedBox(height: 10),
        Text(
          _name,
          style: Helper.getTheme(context)
              .headline4
              !.copyWith(color: AppColor.primary),
        ),
        Row(
          children: [
            Text(_type1),
            SizedBox(
              width: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Text(
                ".",
                style: TextStyle(
                  color: AppColor.orange,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Text(_type2),
            SizedBox(
              width: 20,
            ),
            Icon(Icons.star,color: AppColor.orange,),
            SizedBox(
              width: 5,
            ),
            Text(
              "4.9",
              style: TextStyle(
                color: AppColor.orange,
              ),
            )
          ],
        )
      ],
    );
  }
}



