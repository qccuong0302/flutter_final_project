import 'package:flutter/material.dart';
import '../data/foodlist.dart';

import '../const/color.dart';
import '../widget/customNavBar.dart';

class BeveragePage extends StatelessWidget {
  const BeveragePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beverage"),
        backgroundColor: AppColor.orange,

      ),
      body: Body(),
    );
  }
  Widget Body() {
    return Stack(
      children: [
        SafeArea(
          child: Container(
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            child: ListView.separated(

              padding: const EdgeInsets.all(20),
              itemCount: beverageList.length,
              itemBuilder: (context,index){
                return ListTile(
                  leading: CircleAvatar(backgroundImage: AssetImage(beverageList[index]['img']),),
                  title: Text(beverageList[index]['name'],style:TextStyle(color: Colors.black,fontSize: 20)),
                  subtitle: Text(beverageList[index]['type2']),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(color: Colors.white,);
              },
            ),
          ),
        ),
        Positioned(
            bottom: 0,
            left: 0,
            child: CustomNavBar(
              home: true,
            )),

      ],
    );





  }
}
