import 'package:flutter/material.dart';
import '../const/color.dart';
import '../const/helper.dart';
import '../widget/customNavBar.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios_rounded,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Notifications",
                            style: Helper.getTheme(context).headline5,
                          ),
                        ),
                        Image.asset(
                          Helper.getAssetName("cart.png", "icon"),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(child: ListTile(
                    title: Text("Your order has been picked up"),
                    subtitle: Text("Now"),
                  )
                  ),
                  Card(child: ListTile(
                    title: Text("Your order has been picked up"),
                    subtitle: Text("1h ago"),
                  )
                  ),
                  Card(child: ListTile(
                    title: Text("Your order has been picked up"),
                    subtitle: Text("3h ago"),
                  )
                  ),
                  Card(child: ListTile(
                    title: Text("Your order has been picked up"),
                    subtitle: Text("5h ago"),
                  )
                  ),
                  Card(child: ListTile(
                    title: Text("Your order has been picked up"),
                    subtitle: Text("30 April 2022"),
                  )
                  ),
                ],
              )),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                menu: true,
              ))
        ],
      ),
    );
  }
}

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: AppColor.placeholder,
            width: 0.5,
          ),
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundColor: AppColor.orange,
            radius: 5,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
          )
        ],
      ),
    );
  }

