import 'dart:ui';
import 'package:flutter/material.dart';
import '../pages/homepage.dart';
import 'package:intro_slider/intro_slider.dart';
class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = [];

  @override
  void initState() {
    super.initState();

    slides.add(
      Slide(
        title: "Find Food You Love",
        description:
        "Allow miles wound place the leave had. To sitting subject no improve studied limited",
        pathImage: "assets/images/intro/intro1.png",
        backgroundColor: const Color(0xfff5a623),
      ),
    );
    slides.add(
      Slide(
        title: "Fast Delivery",
        description:
        "Fast food delivery to your home, office wherever you are",
        pathImage: "assets/images/intro/intro2.png",
        backgroundColor: const Color(0xff203152),
      ),
    );
    slides.add(
      Slide(
        title: "Live Tracking",
        description:
        "Real time tracking of your food on the app once you placed the order",
        pathImage: "assets/images/intro/intro3.png",
        backgroundColor: const Color(0xff9932CC),
      ),
    );
  }

  void onDonePress() {
    // Do what you want
      Navigator.push(context, MaterialPageRoute(builder: (context)=>const HomePage()));
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: slides,
      onDonePress: onDonePress,
    );
  }
}


