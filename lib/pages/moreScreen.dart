import 'package:flutter/material.dart';
import 'aboutScreen.dart';
import 'inboxScreen.dart';
import 'myOrderScreen.dart';
import 'notificationScreen.dart';
import 'PaymentScreen.dart';
import 'package:flutter_final_project/const/color.dart';
import 'package:flutter_final_project/const/helper.dart';
import 'package:flutter_final_project/pages/PaymentScreen.dart';
import 'package:flutter_final_project/widget/customNavBar.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SingleChildScrollView(
                child: Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "More",
                        style: Helper.getTheme(context).headline5,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const PaymentScreen())) ;
                    },
                    child: MoreCard(
                        image: Image.asset(
                          Helper.getAssetName("income.png", "icon"),
                        ),
                        name: "Payment Details",

                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>const MyOrderScreen()));

                    },
                    child: MoreCard(
                      image: Image.asset(
                        Helper.getAssetName("shopping_bag.png", "icon"),
                      ),
                      name: "My Orders",

                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap:(){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>const NotificationScreen()));

                    } ,
                    child: MoreCard(
                      image: Image.asset(
                        Helper.getAssetName("noti.png", "icon"),
                      ),
                      name: "Notifications",
                      isNoti: true,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>const InboxScreen()));
                    },
                    child: MoreCard(
                      image: Image.asset(
                        Helper.getAssetName("mail.png", "icon"),
                      ),
                      name: "Inbox",
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>const AboutScreen()));

                    },
                    child: MoreCard(
                      image: Image.asset(
                        Helper.getAssetName("info.png", "icon"),
                      ),
                      name: "About Us",
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ]),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: CustomNavBar(
              more: true,
            ),
          )
        ],
      ),
    );
  }
}

class MoreCard extends StatelessWidget {
  const MoreCard({
    Key? key,
    required String name,
    required Image image,
    bool isNoti = false,

  })  : _name = name,
        _image = image,
        _isNoti = isNoti,

        super(key: key);

  final String _name;
  final Image _image;
  final bool _isNoti;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        height: 70,
        width: double.infinity,
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              margin: const EdgeInsets.only(
                right: 20,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                color: AppColor.placeholderBg,
              ),
              child: Row(
                children: [
                  Container(
                      width: 50,
                      height: 50,
                      decoration: ShapeDecoration(
                        shape: CircleBorder(),
                        color: AppColor.placeholder,
                      ),
                      child: _image),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    _name,
                    style: TextStyle(
                      color: AppColor.primary,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 30,
                width: 30,
                decoration: ShapeDecoration(
                  shape: CircleBorder(),
                  color: AppColor.placeholderBg,
                ),
                child: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: AppColor.secondary,
                  size: 17,
                ),
              ),
            ),
            if (_isNoti)
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 20,
                  width: 20,
                  margin: const EdgeInsets.only(
                    right: 50,
                  ),
                  decoration: ShapeDecoration(
                    shape: CircleBorder(),
                    color: Colors.red,
                  ),
                  child: Center(
                    child: Text(
                      "5",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
