import 'package:flutter/material.dart';
import '../const/color.dart';

import '../const/helper.dart';
import '../widget/customNavBar.dart';
import 'myOrderScreen.dart';

class FoodDetail extends StatefulWidget {
  final String name;
  final String type2;
  final String img;
  final String description;

  const FoodDetail({Key? key, required this.name, required this.type2, required this.img, required this.description}) : super(key: key);

  @override
  State<FoodDetail> createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.orange,
        actions: [
          IconButton(onPressed: (){}, icon: Icon(Icons.shopping_cart))
        ],
      ),
      body: Body(),

    );
  }
  Widget Body(){
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Stack(
        children: [
          SizedBox(height: 30,),
          SafeArea(

            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
                    child: Container(
                      width: size.width-60,
                      height: size.width-150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(widget.img),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(15)
                      ),
                    ),
                  ),
                  SizedBox(height: 30,),
                  Container(
                    width: size.width - 80,
                    height: 120,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 200,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                widget.name,
                                style: TextStyle(
                                    fontSize:20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(widget.type2,
                                style: TextStyle(
                                    fontSize:15,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Image.asset(
                                  Helper.getAssetName(
                                      "star_filled.png",
                                      "icon"),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Image.asset(
                                  Helper.getAssetName(
                                      "star_filled.png",
                                      "icon"),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Image.asset(
                                  Helper.getAssetName(
                                      "star_filled.png",
                                      "icon"),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Image.asset(
                                  Helper.getAssetName(
                                      "star_filled.png",
                                      "icon"),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Image.asset(
                                  Helper.getAssetName(
                                      "star.png",
                                      "icon"),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "4 Star Ratings",
                              style: TextStyle(
                                color: AppColor.orange,
                                fontSize: 12,
                              ),
                            )
                          ],
                        ),

                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Text(
                      "Description",
                      style: Helper.getTheme(context)
                          .headline4
                          !.copyWith(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Text(widget.description),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Divider(
                      color: AppColor.placeholder,
                      thickness: 1.5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Text(
                      "Customize your Order",
                      style: Helper.getTheme(context)
                          .headline4
                          !.copyWith(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Container(
                      height: 50,
                      width: size.width,
                      padding: const EdgeInsets.only(
                          left: 30, right: 10),
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.circular(5),
                        ),
                        color: AppColor.placeholderBg,
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Row(
                            children: [
                              Text(
                                  "-Select the size of portion-"),
                            ],
                          ),
                          value: "default",
                          onChanged: (_) {},
                          items: [
                            DropdownMenuItem(
                              child: Text(
                                  "-Select the size of portion-"),
                              value: "default",
                            ),
                          ],
                          icon: Image.asset(
                            Helper.getAssetName(
                              "dropdown.png",
                              "icon",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Container(
                      height: 50,
                      width: size.width,
                      padding: const EdgeInsets.only(
                          left: 30, right: 10),
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.circular(5),
                        ),
                        color: AppColor.placeholderBg,
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Row(
                            children: [
                              Text(
                                  "-Select the ingredients-"),
                            ],
                          ),
                          value: "default",
                          onChanged: (_) {},
                          items: [
                            DropdownMenuItem(
                              child: Text(
                                  "-Select the ingredients-"),
                              value: "default",
                            ),
                          ],
                          icon: Image.asset(
                            Helper.getAssetName(
                              "dropdown.png",
                              "icon",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20),
                    child: Row(
                      children: [
                        Text(
                          "Number of Portions",
                          style: Helper.getTheme(context)
                              .headline4
                              !.copyWith(
                            fontSize: 16,
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.end,
                            children: [
                              ElevatedButton(

                                style: ButtonStyle(
                                    backgroundColor:MaterialStateProperty.all<Color>(Colors.orange),
                                    elevation:
                                    MaterialStateProperty
                                        .all(5.0)),
                                onPressed: () {},
                                child: Text("-"),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                height: 35,
                                width: 30,
                                decoration:
                                ShapeDecoration(
                                  shape: StadiumBorder(
                                    side: BorderSide(
                                        color: AppColor
                                            .orange),
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment:MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "2",
                                      style: TextStyle(
                                        color: AppColor
                                            .orange,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:MaterialStateProperty.all<Color>(Colors.orange),
                                    elevation:
                                    MaterialStateProperty.all(5.0)),
                                onPressed: () {},
                                child: Text("+"),
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                  SizedBox(
                    height: 200,
                    width: size.width,
                    child: Stack(
                      children: [
                        Container(
                          width: 120,
                          decoration: ShapeDecoration(
                            color: AppColor.orange,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.only(
                                topRight:
                                Radius.circular(40),
                                bottomRight:
                                Radius.circular(40),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment:
                          Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets
                                .symmetric(
                              horizontal: 20,
                            ),
                            child: Container(
                              height: 160,
                              width: double.infinity,
                              margin:
                              const EdgeInsets.only(
                                left: 50,
                                right: 40,
                              ),
                              decoration: ShapeDecoration(
                                shape:
                                RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.only(
                                    topLeft:
                                    Radius.circular(
                                        40),
                                    bottomLeft:
                                    Radius.circular(
                                        40),
                                    topRight:
                                    Radius.circular(
                                        10),
                                    bottomRight:
                                    Radius.circular(
                                        10),
                                  ),
                                ),
                                shadows: [
                                  BoxShadow(
                                    color: AppColor
                                        .placeholder
                                        .withOpacity(0.3),
                                    offset: Offset(0, 5),
                                    blurRadius: 5,
                                  ),
                                ],
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                MainAxisAlignment
                                    .center,
                                children: [
                                  Text(
                                    "Total Price",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "150.000 VND",
                                    style: TextStyle(
                                      color: AppColor
                                          .primary,
                                      fontWeight:
                                      FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  SizedBox(
                                    width: 200,
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>const MyOrderScreen()));
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                              .center,
                                          children: [
                                            Image.asset(
                                              Helper.getAssetName(
                                                  "add_to_cart.png",
                                                  "icon"),
                                            ),
                                            Text(
                                              "Add to Cart",
                                            )
                                          ],
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            right: 20,
                          ),
                          child: Align(
                            alignment:
                            Alignment.centerRight,
                            child: Container(
                              width: 60,
                              height: 60,
                              decoration: ShapeDecoration(
                                color: Colors.white,
                                shadows: [
                                  BoxShadow(
                                    color: AppColor
                                        .placeholder
                                        .withOpacity(0.3),
                                    offset: Offset(0, 5),
                                    blurRadius: 5,
                                  ),
                                ],
                                shape: CircleBorder(),
                              ),
                              child:  IconButton(onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) =>const MyOrderScreen()));

                              }, icon: Icon(Icons.shopping_cart),color: AppColor.orange,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),






                ],

              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: CustomNavBar(),
          ),


        ],
      ),
    );
  }
}
