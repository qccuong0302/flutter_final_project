import 'package:flutter/material.dart';
import '../data/foodlist.dart';
import '../const/color.dart';
import '../widget/customNavBar.dart';

class PopularPage extends StatelessWidget {
  const PopularPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Popular"),
        backgroundColor: AppColor.orange,

      ),
      body: Body(),
    );
  }
  Widget Body() {
    return Stack(
      children: [
        SafeArea(
          child: Container(
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            child: ListView.separated(

              padding: const EdgeInsets.all(20),
              itemCount: delicacieslist.length,
              itemBuilder: (context,index){
                return ListTile(
                  leading: CircleAvatar(backgroundImage: AssetImage(delicacieslist[index]['img']),),
                  title: Text(delicacieslist[index]['name'],style:TextStyle(color: Colors.black,fontSize: 20)),
                  subtitle: Text(delicacieslist[index]['type2']),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(color: Colors.white,);
              },
            ),
          ),
        ),
        Positioned(
            bottom: 0,
            left: 0,
            child: CustomNavBar(
              home: true,
            )),

      ],
    );





  }
}
