const List delicacieslist = [
  {
    "img": "assets/images/food/mc_burger.png",
    "name": "Big Mac",
    "type1":"Hamburger",
    "type2":"Mc Donald's",
    "description":"Big Burger with beef and cheese"
  },
  {
    "img": "assets/images/food/tiramisu.jpg",
    "name": "Tiramisu",
    "type1":"Dessert",
    "type2":"Girval Bakery",
    "description":"Tiramisu is an elegant and rich layered Italian dessert made with delicate ladyfinger cookies, espresso or instant espresso, mascarpone cheese, eggs, sugar, Marsala wine, rum and cocoa powder. Through the grouping of these diverse ingredients, an intense yet refined dish emerges.",
  },
  {
    "img": "assets/images/food/comtam.png",
    "name": "Cơm sườn chả trứng",
    "type1":"Rice",
    "type2":"Sà Bì Chưởng Restaurant",
    "description":"Cơm sườn, chả, trứng, nước tùy chọn và khăn lạnh",
  },
  {
    "img": "assets/images/food/bunbo.jpg",
    "name": "Bún bò thập cẩm",
    "type1":"Noddle",
    "type2":"Cô Như",
    "description":"Bún bò nạm, bắp, gân, chả, giò",
  },
  {
    "img": "assets/images/drink/milktea.jpg",
    "name": "Trà sữa",
    "type1":"Milktea",
    "type2":"Gongcha Milktea",
    "description":"Bubble tea brings together the finest tea in the tastiest varieties with amazing add-ins like tapioca pearls or delightful fruit to create a one-of-a-kind drink experience and you will be clamoring to share with friends, family and social media followers.",
  },
  {
    "img": "assets/images/drink/luckytea.jpg",
    "name": "Trà Thảo Mộc",
    "type1":"Tea",
    "type2":"Phúc Long",
    "description":"Best Seller in Summer",
  },
  {
    "img": "assets/images/drink/bubble_milk_tea.jpg",
    "name": "Trà sữa trân châu",
    "type1":"Milktea",
    "type2":"Koi Thé",
    "description":"",
  },
  {
    "img": "assets/images/food/mi.jpg",
    "name": "Mì Trộn Muối Ớt",
    "type1":"Noddle",
    "type2":"Tên Lửa",
    "description":"",
  },
  {
    "img": "assets/images/food/western.png",
    "name": "Gà rán truyền thống",
    "type1":"Fried Chicken",
    "type2":"Texas Chicken",
    "description":"2 miếng gà có xương, 1 nước ngọt, 2 tương ớt + 1 tương cà",
  },
];
const List foodList = [
  {
    "img": "assets/images/food/bc.png",
    "name": "Bánh căn",
    "type2":"Nha Trang",

  },
  {
    "img": "assets/images/food/mc_burger.png",
    "name": "Big Mac",
    "type1":"Hamburger",
    "type2":"Mc Donald's",
    "description":"Big Burger with beef and cheese"
  },
  {
    "img": "assets/images/food/hutieu.png",
    "name": "Hủ tiếu",
    "type1":"Rice",
    "type2":"Hủ tiếu Thành Đạt",

  },
  {
    "img": "assets/images/food/mi.jpg",
    "name": "Mì Trộn Muối Ớt",
    "type1":"Noddle",
    "type2":"Tên Lửa",
  },
  {
    "img": "assets/images/food/bdc.png",
    "name": "Bánh Đa Cua",
    "type1":"Noddle",
    "type2":"Miền Trung",
  },
  {
    "img": "assets/images/food/bdmt.png",
    "name": "Bún Đậu Mắm Tôm ",
    "type1":"Noddle",
    "type2":"A Chảnh Quán",
  },
  {
    "img": "assets/images/food/western.png",
    "name": "Gà rán truyền thống",
    "type1":"Fried Chicken",
    "type2":"Texas Chicken",
    "description":"2 miếng gà có xương, 1 nước ngọt, 2 tương ớt + 1 tương cà",
  },
];
const List dessertlist = [
  {
    "img": "assets/images/food/tiramisu.jpg",
    "name": "Tiramisu",
    "type1":"Dessert",
    "type2":"Girval Bakery",
  },
  {
    "img": "assets/images/food/bn1.png",
    "name": "Bánh sô cô la",
    "type1":"Dessert",
    "type2":"Kinh Đô",
  },
  {
    "img": "assets/images/food/bn2.png",
    "name": "Bánh Donut",
    "type1":"Dessert",
    "type2":"Thế Giới Donut",
  },
  {
    "img": "assets/images/food/bn3.png",
    "name": "Strawberry Cake",
    "type1":"Dessert",
    "type2":"Another Bakery",
  },
  {
    "img": "assets/images/food/bn4.png",
    "name": "Bánh sừng trâu trứng muối",
    "type1":"Dessert",
    "type2":"BreadTalk Bakery",
  },
  {
    "img": "assets/images/food/bn5.png",
    "name": "Red Velvet Cake",
    "type1":"Dessert",
    "type2":"Starbuck Vietnam",
  },

];
const List beverageList = [
  {
    "img": "assets/images/drink/milktea.jpg",
    "name": "Trà sữa",
    "type1":"Milktea",
    "type2":"Gongcha Milktea",
  },
  {
    "img": "assets/images/drink/luckytea.jpg",
    "name": "Trà Thảo Mộc",
    "type1":"Tea",
    "type2":"Phúc Long",
  },
  {
    "img": "assets/images/drink/bubble_milk_tea.jpg",
    "name": "Trà sữa trân châu",
    "type1":"Milktea",
    "type2":"Koi Thé",
  },
  {
    "img": "assets/images/drink/bia.png",
    "name": "Budweiser",
    "type1":"Beer",
    "type2":"BachHoaXanh",
  },
  {
    "img": "assets/images/drink/gc.png",
    "name": "Trà sữa trân châu",
    "type1":"Milktea",
    "type2":"GongCha",
  },
  {
    "img": "assets/images/drink/ne.png",
    "name": "Nước ép các loại",
    "type1":"Juice",
    "type2":"Sinh tố Hồng",
  },
  {
    "img": "assets/images/drink/ruou1.png",
    "name": "Jaegermeister",
    "type1":"Wine",
    "type2":"BachHoaXanh",
  },
  {
    "img": "assets/images/drink/tra.png",
    "name": "Trà hoa cúc",
    "type1":"Tea",
    "type2":"The Coffee House",
  },


];
const List freeshipList = [
  {
    "img": "assets/images/food/mc_burger.png",
    "name": "Big Mac",
    "type1":"Hamburger",
    "type2":"Mc Donald's",
  },
  {
    "img": "assets/images/food/tiramisu.jpg",
    "name": "Tiramisu",
    "type1":"Dessert",
    "type2":"Girval Bakery",
  },
  {
    "img": "assets/images/food/comtam.png",
    "name": "Cơm sườn chả trứng",
    "type1":"Rice",
    "type2":"Xà Bì Chưởng Restaurant",
  },
  {
    "img": "assets/images/food/bunbo.jpg",
    "name": "Bún bò thập cẩm",
    "type1":"Noddle",
    "type2":"Cô Như",
  },
  {
    "img": "assets/images/drink/milktea.jpg",
    "name": "Trà sữa",
    "type1":"Milktea",
    "type2":"Gongcha Milktea",
  },
  {
    "img": "assets/images/drink/luckytea.jpg",
    "name": "Trà Thảo Mộc",
    "type1":"Tea",
    "type2":"Phúc Long",
  },
  {
    "img": "assets/images/drink/bubble_milk_tea.jpg",
    "name": "Trà sữa trân châu",
    "type1":"Milktea",
    "type2":"Koi Thé",
  },
  {
    "img": "assets/images/food/western.png",
    "name": "Gà rán truyền thống",
    "type1":"Fried Chicken",
    "type2":"Texas Chicken",
  },
  {
    "img": "assets/images/drink/ne.png",
    "name": "Nước ép các loại",
    "type1":"Juice",
    "type2":"Sinh tố Hồng",
  },
  {
    "img": "assets/images/drink/ruou1.png",
    "name": "Jaegermeister",
    "type1":"Wine",
    "type2":"BachHoaXanh",
  },
  {
    "img": "assets/images/food/bn4.png",
    "name": "Bánh sừng trâu trứng muối",
    "type1":"Dessert",
    "type2":"BreadTalk Bakery",
  },
  {
    "img": "assets/images/food/bn5.png",
    "name": "Red Velvet Cake",
    "type1":"Dessert",
    "type2":"Starbuck Vietnam",
  },
];

