import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_final_project/pages/homepage.dart';
import 'package:flutter_final_project/pages/intropage.dart';
import 'package:intro_slider/intro_slider.dart';
import '../const/color.dart';
import '../shared/firebase_authentication.dart';

import 'dart:ui';
class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<StatefulWidget> {
  String _message = '';
  bool _isLogin = true;
  final TextEditingController txtUserName = TextEditingController();
  final TextEditingController txtPassword = TextEditingController();

  late FirebaseAuthentication auth;

  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      auth = FirebaseAuthentication();
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Login Screen'),
          backgroundColor: AppColor.orange,
          actions: [
            IconButton(
              icon: Icon(Icons.logout),
              onPressed: () {
                auth.logout().then((value) {
                  if (value) {
                    setState(() {
                      _message = 'User Logged Out';
                    });
                  } else {
                    setState(() {
                      _message = 'Unable to Log Out';
                    });
                  }
                });
              },
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(36),
          child: ListView(
            children: [
              Image.asset("assets/images/intro/login.png"),

              userInput(),
              passwordInput(),
              btnMain(),
              btnSecondary(),
              btnGoogle(),
              txtMessage(),
            ],
          ),
        ));
  }

  Widget userInput() {
    return Padding(
        padding: EdgeInsets.only(top: 0),
        child: TextFormField(
          controller: txtUserName,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
              filled: true,
              fillColor: AppColor.placeholderBg,
              hintText: 'User Name', icon: Icon(Icons.verified_user, color: AppColor.secondary,),hintStyle: TextStyle(color: AppColor.secondary),
              enabledBorder:OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)
              ) ),
          validator: (text) => text!.isEmpty ? 'User Name is required' : '',
        ));
  }

  Widget passwordInput() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: TextFormField(
          controller: txtPassword,
          keyboardType: TextInputType.emailAddress,
          obscureText: true,
          decoration: InputDecoration(
              filled: true,
              fillColor: AppColor.placeholderBg,
              hintText: 'password', icon: Icon(Icons.enhanced_encryption, color: AppColor.secondary), hintStyle: TextStyle(color: AppColor.secondary),
              enabledBorder:OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)
              )),
          validator: (text) => text!.isEmpty ? 'Password is required' : '',
        ));
  }

  Widget btnMain() {
    String btnText = _isLogin ? 'Log in' : 'Sign up';
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
            height: 45,
            child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:MaterialStateProperty.all<Color>(AppColor.orange) ,
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24.0),
                        side: BorderSide(color: Colors.white)),
                  ),
                ),
                child: Text(
                  btnText,
                  style: TextStyle(
                      fontSize: 18, color: Colors.white),
                ),
                onPressed: () {
                  String userId = '';
                  if (_isLogin) {
                    auth.login(txtUserName.text, txtPassword.text).then((value) {
                      if (value == null) {
                        setState(() {
                          _message = 'Login Error';
                        });
                      } else {
                        userId = value;
                        setState(() {
                          _message = 'User $userId successfully logged in';
                        });
                        changeScreen();
                      }
                    });
                  } else {
                    auth.createUser(txtUserName.text, txtPassword.text).then((value) {
                      if (value == null) {
                        setState(() {
                          _message = 'Registration Error';
                        });

                      } else {
                        userId = value;
                        setState(() {
                          _message = 'User $userId successfully signed in';
                        });
                        changeScreen();
                      }
                    });
                  }
                })));
  }

  Widget btnSecondary() {
    String buttonText = _isLogin ? 'Sign up' : 'Log In';
    return TextButton(
      style: TextButton.styleFrom(primary: AppColor.orange),
      child: Text(buttonText),
      onPressed: () {
        setState(() {
          _isLogin = !_isLogin;
        });
      },
    );
  }

  Widget txtMessage() {
    return Text(
      _message,
      style: TextStyle(
          fontSize: 16,
          color: Colors.green,
          fontWeight: FontWeight.bold),
    );
  }

  Widget btnGoogle() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          height: 45,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor:MaterialStateProperty.all<Color>(Colors.red) ,
              shape: MaterialStateProperty.all
              <RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24.0),
                    side: BorderSide(color: Colors.white)),
              ),
            ),
            onPressed: () {
              auth.loginWithGoogle().then((value) {
                if (value == null) {
                  setState(() {
                    _message = 'Google Login Error';
                  });

                } else {
                  setState(() {
                    _message = 'User $value successfully logged in with Google';
                  });
                  changeScreen();
                }

              });
            },
            child: Text(
              'Log in with Google',
              style: TextStyle(
                  fontSize: 18, color:Colors.white
              ),
            ),
          ),
        ));
  }

  void changeScreen() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => IntroScreen()));
  }
}


